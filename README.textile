h1=. DPTopTabsViewController

p=. Version 0.9 -- 29 August 2012

p=. By "Diego Peinador":mailto:diegopeinador@gmail.com

h2. Introduction

*DPTopTabsViewController* is an attempt to create a container view controller similar to those found in some web browser: Tabs on the top of the view that change the view controllers inside.

h3. Features

* High level of personalization: you can change colors, sizes, overlapping of tabs...
* You can use images like in a UISegmentedControl for the background (in this case, height is fixed by the image, and overlapping is zero).
* Animations for inserting/removing view controllers (not with background images).
* Unlimited number of tabs, if they don't fit a scroll view will be used.
* Cross-fade effect when changing tabs.
* Changes in contained view controllers' titles are reflected in the tab

h3. Requirements

* This project uses ARC, so you'll be better with iOS 5.0 SDK, also I may have forgotten some @synthesize so you may want to use Xcode 4.4.
* Haven't tested in an iPhone yet, It should be compatible, though.

h3. How to get it

Download or clone "this repo":https://bitbucket.org/diegopeinador/dptoptabsviewcontroller

h3. How to use it in your project

You only need to include DPTopTabsViewController in your project, the rest of this repo is a test project, so you can see some examples of customization.

You can @#impor "DPTopTabsViewController.h"@ setup the view (i.e. in storyboard) and @addViewController:animated:@. Then you can start, customizing.
