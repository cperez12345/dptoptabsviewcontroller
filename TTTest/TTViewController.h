//
//  TTViewController.h
//  TTTest
//
//  Created by Diego Peinador on 06/08/12.
//  Copyright (c) 2012 iPhoneDroid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *testView;
@property (weak, nonatomic) IBOutlet UITextField *vcId;
@property (weak, nonatomic) IBOutlet UILabel *overlapLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;

- (IBAction)addVC:(id)sender;
- (IBAction)removeVC:(id)sender;
- (IBAction)updateTitleHeight:(id)sender;
- (IBAction)changeAligment:(id)sender;
- (IBAction)changeColor:(id)sender;
- (IBAction)updateTitleOverlap:(id)sender;
- (IBAction)useImages:(id)sender;
- (IBAction)reset:(id)sender;
- (IBAction)changeTextColor:(id)sender;

@end
